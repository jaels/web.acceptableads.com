
01. {{acceptable-ads-are-1[Acceptable Ads are 01:] Are not annoying.}}
02. {{acceptable-ads-are-2[Acceptable Ads are 02:] Do not disrupt or distort the page content we're trying to read}}
03. {{acceptable-ads-are-3[Acceptable Ads are 03:] Are transparent with us about being an ad}}
04. {{acceptable-ads-are-4[Acceptable Ads are 04:] Are effective without shouting at us}}
05. {{acceptable-ads-are-5[Acceptable Ads are 05:] Are appropriate to the site that we are on}}

{{acceptable-ads-are-p[Acceptable Ads... paragraph] We developed the original criteria with our users, refined them based upon ​eyetracking research and ​surveys and are giving them to an ​independent committee, which will oversee all future improvements to them.}}

[{{view-criteria-button[View criteria button label] View criteria}}](about/criteria){: .btn-outline-primary }
{: .btn-container }
