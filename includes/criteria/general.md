
# {{heading[Heading text] Criteria}}

{{fairness-rules[Text below the heading] To ensure transparency and fairness, the following rules have and will always apply to everyone without exceptions:}}

* {{avoid-criteria[bullet point text in top section] Participants cannot pay to avoid the criteria. <strong>Every ad has to comply with the criteria.</strong>}}
* {{transparency[bullet point text in top section] For transparency reasons, we add all Acceptable Ads to [our forum](https://adblockplus.org/forum/viewforum.php?f=12&sid=14f7543363191f4c8be21b72af6f956c) to provide our community with the opportunity to submit feedback. We greatly value feedback and read all comments.}}
* {{user-value[bullet point text in top section] Adblock Plus users are valuable to us and we listen to them. If, for valid reasons, any Acceptable Ads proposal is rejected by our community, the ad(s) will be removed from our whitelist.}}

{{charging[Paragraph text in top section] We are able to keep our open source product free [by charging large entities a fee](https://adblockplus.org/about#monetization) for whitelisting services. For the other 90% of our partners, these services are offered free of charge.}}
