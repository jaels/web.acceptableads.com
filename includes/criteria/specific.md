# {{specific-criteria-heading[Specefic criteria heading] Specific criteria}} {: #specific-criteria }

#### {{text-ads[Text in "Specific criteria"] Text ads}} {: .item #text-criteria }

  {{attention-grab["Text ads" description in "Specific criteria"] Text ads designed with excessive use of colors and/or other elements to grab attention are not permitted.}}

#### {{image-ads[Subheadline in "Specific criteria"] Image ads}} {: .item #image-criteria }

{{unobtrusive-image["Image ads" description in "Specific criteria"] Static image ads may qualify as acceptable, according to an evaluation of their unobtrusiveness based on their integration on the webpage.}}

#### {{in-feed-ads[Subheadline in "Specific criteria"] In-feed ads}} {: .item #feed-criteria }

{{feed-list-ads["In-feed ads" description in "Specific criteria"] For ads in lists and feeds, the general criteria differ depending on:}}

##### {{placement-requirement[Bullet point in "Specific criteria"] Placement requirements}} {: #feed-ads-placement }

{{entries-feeds[Text below bullet point in "Specific criteria"] Ads are permitted in between entries and feeds.}}

##### {{size-requirment[Bullet point in "Specific criteria"] Size requirements}} {: #feeds-size-requirement}

{{in-feed-ads-size[Text below bullet point in "Specific criteria"] In-feed ads are permitted to take up more space, as long as they are not substantially larger than other elements in the list or feed.}}

#### {{search-ads[Subheadline in "Specific criteria"] Search ads }} {: .item #search-criteria }

{{search-ads-info["Search ads" description in "Specific criteria"] For search ads - ads displayed following a user-initiated search query - the criteria differ depending on:}}

##### {{size-requirement[Bullet point in "Specific criteria"] Size requirements}} {: #search-ads-size }

{{more-space[Text below bullet point in "Specific criteria"] Search ads are permitted to be larger and take up additional screen space.}}

#### {{no-primary-content[Subheadline in "Specific criteria"] Ads on pages with no primary content }} {: .item #no-content }

{{only-text-ads["Ads on pages with no primary content" description in "Specific criteria"] Only text ads are allowed. For webpages without any primary content (e.g. error or parking pages), the criteria differ depending on:}}

##### {{placement-requirement Placement requirements}} {: #no-content-placement }

{{no-placement-limit[Text below point in "Specific criteria"] No placement limitations.}}

##### {{size-requirement}} {: #no-content-size-requirement }

{{no-size-limit[Text below bullet point in "Specific criteria"] No size limitations.}}

#### {{other-acceptable-ads[Subheadline in "Specific criteria"] Other Acceptable Ads formats? }} {: .item #other-formats }

{{new-format["Other Acceptable Ads formats" text in "Specific criteria"] Are your ads displayed on alternative screens, or are you convinced that you have an innovative Acceptable Ads format which doesn't fit the ads outlined above? <a href="mailto:acceptableads@adblockplus.org">Let us know!</a>}}
