<div class="card" markdown="1">

<header class="card-heading" markdown="1">
### {{abb-android-heading[Acceptable Ads SUPPORTED PRODUCTS > heading] <fix>Adblock Browser</fix> for <fix>Android</fix> }}
</header>

{{abb-android-body[Acceptable Ads SUPPORTED PRODUCTS > body] Mobile browser for <fix>Android OS</fix> versions 2.3 and higher }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{download[Text for download button] Download}}](https://adblockbrowser.org?product=abb-android){: .btn-outline-primary }
</footer>

</div>
