<div class="card" markdown="1">

<header class="card-heading" markdown="1">
### {{abp-desktop-heading[Acceptable Ads SUPPORTED PRODUCTS > heading] <fix>Adblock Plus</fix> for desktop browsers }}
</header>

{{abp-desktop-body[Acceptable Ads SUPPORTED PRODUCTS > body] Available for <fix>Firefox</fix>, <fix>Chrome</fix>, <fix>Safari</fix>, <fix>Opera</fix>, <fix>Maxthon</fix>, <fix>Internet Explorer</fix> and <fix>Edge</fix> }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{download[Text for download button] Download }}](https://adblockplus.org/){: .btn-outline-primary }
</footer>

</div>