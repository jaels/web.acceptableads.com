<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{users-icon-alt[Users icon alt text] Users icon}}](/img/png/icon-users.png)
{: .card-icon }

### {{users-heading[Who benefits -> Users heading] Users }}
</header>

{{users-body[Who benefits -> Users body] Support the free and open web }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{users-learn-more[Who benefits -> users learn more button] Learn more }}](users){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted){: .btn-primary }
</footer>

</div>
