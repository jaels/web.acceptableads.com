
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{ad-networks-icon-alt[Ad Networks icon alt text] Ad Networks icon}}](/img/png/icon-ad-networks.png)
{: .card-icon }

## {{ad-networks-heading[Ad networks heading] Ad Networks}}
</header>

<div class="card-summary" markdown="1">

{{ad-networks-p1[Ad Networks paragraph 1] Offer a sustainable industry solution}}

{{ad-networks-p2[Ad Networks paragraph 2] Provide additional value for your publishers and advertisers while respecting users’ choice}}

</div>

<footer class="card-footer" markdown="1">
[{{ad-networks-learn-more-button[Ad Networks learn more button] Learn more}}](solutions/ad-networks){: .btn-outline-primary }

[{{ad-networks-inquire-button[Ad Networks inquire button] Get whitelisted}}](get-whitelisted?type=ad-networks){: .btn-primary }
</footer>

</div>
