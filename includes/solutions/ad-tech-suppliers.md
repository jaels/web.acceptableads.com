
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{ad-tech-suppliers-icon-alt[Ad-Tech Suppliers icon alt text] Ad-Tech Suppliers icon}}](/img/png/icon-ad-tech-suppliers.png)
{: .card-icon }

## {{ad-tech-heading[Ad-tech Suppliers heading] Ad-Tech Suppliers}}
</header>

<div class="card-summary" markdown="1">

{{ad-tech-p1[Ad-tech Suppliers paragraph 1] Use opportunities for growth}}

{{ad-tech-p2[Ad-tech Suppliers paragraph 2] Be a key player within the Acceptable Ad tech ecosystem and help building an industry standard}}

</div>

<footer class="card-footer" markdown="1">
[{{ad-tech-learn-more-button[Advertisers learn more button] Learn more}}](solutions/ad-tech-suppliers){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted?type=ad-tech-suppliers){: .btn-primary }
</footer>

</div>
