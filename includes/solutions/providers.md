<aside class="card list" markdown="1">

## {{providers-header[Acceptable Ads providers heading] Acceptable Ads Providers}}

{{providers-summary[Acceptable Ads providers summary] **Interested?** These providers are already serving Acceptable Ads:}}

- [Criteo](http://www.criteo.com/contact-us/){: target=blank }
- [Taboola](https://www.taboola.com/contact){: target=blank }
- [Outbrain](http://www.outbrain.com/contact){: target=blank }
- [Plista](https://www.plista.com/about/contact/){: target=blank }
- [Ligatus](https://www.ligatus.com/en/contact-us){: target=blank }
- [Relap](https://relap.io/){: target=blank }
- [RevContent](https://www.revcontent.com/signup){: target=blank }
- [Dianomi](http://www.dianomi.com/cms/contact/){: target=blank }
- [Carbon](https://carbonads.net/){: target=blank }
- [AdRecover](http://adrecover.com){: target=blank }

</asides>
