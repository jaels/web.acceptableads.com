title=First update! New committee member announcements and ongoing user search
description=It's been a great first week for the Acceptable Ads Committee (AAC): there are four new members and the search for the golden user continues
featured_img_url=/img/png/blog/acceptable-ads-committee-update.png
featured_img_alt=Acceptable Ads Committee update
author=Ben Williams
committee=true
published_date=2017-03-22
template=blog-entry
breadcrumb=First update
parent=blog/index

These are fast times! The Acceptable Ads Committee (AAC) left the harbor last week, and we've got some updates for you as she sets out upon her maiden voyage.

Our first priority since last week is finding an ordinary user. When we announced the first members of the AAC last week, we made an industry-first call to actual users to fill a seat and be heard by the advertising world. We've received a lot of applications from people across the globe who would bring various specialties. We're currently interviewing candidates, and will have more news soon.

In addition to our search for that one golden user, other organizations have continued to step forward to express their interest in contributing to the AAC. So, over the last sevens days, we have added new members and shuffled a few slots with additions/subtractions.

Altogether we've received over 100 applications so far! Here are a few of the highlights that happened in the last week:

- OKO Digital joined the Ad Tech member group!
- MyFinance Inc. joined the Ad Tech member group!
- Skydeo joined the Ad Tech member group!
- Jay has decided to leave his role as an independent advertising creative on the Acceptable Ads Committee; because of his location the time commitments will be too demanding to make the position tenable. 
- ... but Jon Lefley of Saatchi and Saatchi has taken the empty chair! (Jon was serving as a member of the advertising agency member group representing his role at Saatchi and Saatchi. Now, he will be representing himself alone as an independent advertising creative.)

Most of the applications we have received came from users. As I mentioned above, we're still searching for that user; but even if that is our focus, and many of the other seats filled, there is still lots of room on the AAC. [So keep on applying right here!](https://acceptableads.com/en/committee/apply)

After we find the user we will connect the various member groups and set up a date for the first meeting. Fast times, indeed ...
