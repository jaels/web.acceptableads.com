title=Representing you, the user, on the Acceptable Ads Committee
description=We’ve filled the User Advocate Coalition to assure that you're heard
author=Ben Williams
featured_img_url=/img/png/blog/aac-structure-detailed.png
featured_img_alt=Acceptable Ads Committee structure
committee=true
published_date=2017-06-28
template=blog-entry
breadcrumb=Third update
parent=blog/index

In some  respects the Acceptable Ads Committee (AAC) might look similar to other “boards” or “coalitions.” It consists of  a group of people, who meet in an organized fashion to make decisions. But the critical  aspect that makes it unique in the space where it operates – i.e. online advertising – is that the AAC represents _you_, the internet user, and fights for your rights and privacy against abusive advertising. In fact, the AAC includes an entire user coalition possessing full voting rights. It has teeth.

Today we are pleased to say that we’ve filled that user-side coalition for the first official meeting. The User Advocate Coalition (see image to your right) consists of three individuals from digital rights organizations and one actual user representative. We’ve also added members in other groups since last time. Details on that follow below.

Several of the AAC members have been meeting informally since March, and now the first “official” meeting of the entire committee will take place at the Algonquin Hotel in New York City on July 11. The committee will report after the meeting with the full minutes transparent and open to review by the public.

We’re very excited to welcome the following members to the AAC:

- [Caroline Crandall](https://www.linkedin.com/in/carolinecrandall/) will be the representative of the User group. Caroline came to our attention when she contacted us about a fake news filter she was writing. We began to talk, and as it turned out she had great credentials for representing users: she is a former journalist, a UX designer and a programmer currently getting her Masters of Fine Arts (MFA) at the California College of Arts. That unique blend – of informing people about the world (journalism), mixed with a passion for empowering them to identify fake news and, finally, UX, which is maybe the most pure pursuit of understanding users in the digital world – convinced us she was the one. She also knows a ton about ad blocking.

- Joining Caroline as members in the User group are [Zack Sinclair](https://www.linkedin.com/in/zacksinclair/), who developed the ad blocker FairBlocker, which was designed to balance user and publisher needs; and Leli Schiestl, who works on digital education projects at [Heart of Code](http://heartofcode.org/) as well as in the Physics department at the Free University of Berlin.

- [Paul-Olivier Dehaye](https://twitter.com/podehaye?lang=en) will be one of the representatives from a digital rights organization. Paul is a mathematician at the University of Zurich and the co-founder of [PersonalData.IO](https://personaldata.io/), an organization which seeks to help users regain control of their personal data. In his own words: “I am looking forward to translating research data on user preferences for ads into actionable criteria for all the stakeholders, particularly in the areas of privacy and data protection.”

- [Michael Blend](https://www.linkedin.com/in/blender/), co-founder and President of [System1](http://system1.com/), will join as a member of the Ad-Tech group.

- [Thomas Wrobel](https://www.linkedin.com/in/thowro/?ppe=1), who heads Global Performance Marketing at [Trivago](http://company.trivago.com/), is coming on board as a member to the Advertising group.

- [Zubair Shafiq](https://www.linkedin.com/in/zubair-shafiq-884aa466/), who is an assistant professor at the Department of Computer Science at the University of Iowa and who did a [great study on ad blockers against anti-adblockers](http://homepage.divms.uiowa.edu/~mshafiq/files/adblock-pets2017.pdf), will join the Researcher group. Zubair said he was … “looking forward to working with other members of the committee to help improve ad-block experience for both users and publishers.”

The inclusion of now 38 representatives and members in the AAC, who are all expressly at the table to vote _for users' rights_, is the main reason that the AAC is unique. Also, rather than just take ad _format_ into account, the AAC will consider aspects like privacy and security in ads when making acceptability standards. It is also completely independent – neither we nor other ad blockers that would “enforce” AAC standards for acceptability have a vote. Finally, from a macro level, the AAC’s approach is not to discourage people from ad blocking with walls, coercion, persuasion or deceit: the idea is to let blockers block, but treat this special group to an optional, specialized experience.

We’ll talk to you in mid-July to let you know how it went.
