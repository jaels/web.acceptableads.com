title=Documents | The Acceptable Ads Committee
description=Share, view or download documents.
breadcrumb=Documents
parent=committee/index
custom=1

<style>
th
{
  display: none;
}

tr
{
  border-bottom: 4px solid #FFF;
}

table td
{
  background-color: #F5F5F5;
  border: 0px;
}

@media (max-width: 543px)
{
  table td
  {
    display: block;
  }
}
</style>

<div class="container" markdown="1">

# {{documents-heading[Documents page heading] Documents}}

</div>

<div class="bg-accent p-y-xs p-x-sm" markdown="1">

### Download documents from the committee {: .center .m-y-sm }

</div>

<div class="container p-y-lg" markdown="1">

| Document name                                               | Published date |
| ----------------------------------------------------------- | -------------: |
| [Committee Bylaws 2017](/pdf/acceptable-ads-committee-bylaws.pdf)                | MAR 15, 2017    |
| [Committee Structure 2017](/img/jpg/acceptable-ads-committee-structure.jpg)          | MAR 15, 2017    |

</div>
