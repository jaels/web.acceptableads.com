title=Members | The Acceptable Ads Committee
description=A list of all Representatives and supporting Members that make up the Acceptable Ads Committee.
parent=committee/index
custom=1

{% block head %}
  <style>
    @media(min-width: 768px)
    {
      #committee-structure
      {
        padding-top: 25%;
        margin: 0px auto;
        max-width: 70%;
      }
    }

    .group-list h4
    {
      margin-top: 40px;
      padding: 8px 22px;
      background-color: #E1E1E1;
    }

    .group-list dl
    {
      margin: 22px;
    }

    .group-list dt
    {
      margin: 32px 0px 26px 0px;
      color: #424242;
      text-transform: none;
    }

    .group-list dd
    {
      margin: 0px;
    }

    .member-list
    {
      display: block;
      width: 100%;
      margin: 0px;
      overflow: auto;
    }

    .member-list li
    {
      width: 100%;
      float: left;
      padding-left: 0px;
      margin-bottom: 1em;
    }

    @media(min-width: 768px)
    {
      .member-list li
      {
        min-height: 4em;
        width: 50%;
      }
    }

    @media(min-width: 992px)
    {
      .member-list li
      {
        width: 33.3333333%;
      }
    }

    .member-list li:before
    {
      display: none;
    }
  </style>
{% endblock %}

{% set coalitions = {
  "for-profit": {
    "title": "For-Profit Coalition",
    "description": "This Coalition consists of stakeholders that can be classified as organizations primarily driven by generating profits. The following groups form the For-Profit Coalition: Advertisers, Ad-Tech Agencies, Advertising Agencies, and Publishers and Content Creators.",
    "groups": [
      "Advertisers",
      "Ad Tech Agencies",
      "Advertising Agencies",
      "Publishers & Content Creators",
    ],
  },
  "expert": {
    "title": "Expert Coalition",
    "description": "This Coalition consists of stakeholders who primarily focus on specific issues relevant to their area of expertise. These experts bring a unique and objective perspective to the AAC. The following groups form the Expert Coalition: Creative Agents, Researchers and Academics and User Agents.",
    "groups": [
      "Creative Agents",
      "Researchers & Academics",
      "User Agents",
    ],
  },
  "user-advocate": {
    "title": "User Advocate Coalition",
    "description": "This Coalition consists of stakeholders that can be classified as entities primarily driven by protecting the rights of online users. The following Member Groups form the User Advocate Coalition: Digital Rights Organizations and Individual Users.",
    "groups": [
      "Digital Rights Organizations",
      "Users",
    ],
  },
} %}

{% set members = [
  {
    "group": "Advertisers",
    "position": "Representative",
    "name": "Breanna Fowler",
    "title": "Strategic Partnerships & Advertising",
    "org": "Dell",
  },
  {
    "group": "Advertising Agencies",
    "position": "Representative",
    "name": "Gabriel Cheng",
    "title": "VP Strategy Growth & Partnerships",
    "org": "M&C Saatchi Mobile",
  },
  {
    "group": "Advertising Agencies",
    "position": "Member",
    "name": "Kevin Vanvalkenburgh",
    "title": "Vice President of Connections Planning",
    "org": "The Tombras Group",
  },
  {
    "group": "Advertising Agencies",
    "position": "Member",
    "name": "Tom Rathbone",
    "title": "Head of Technology",
    "org": "PartnerCentric",
  },
  {
    "group": "Advertising Agencies",
    "position": "Member",
    "name": "Christoph Bornschein",
    "title": "CEO/Founder",
    "org": "TLGG",
  },
  {
    "group": "Advertising Agencies",
    "position": "Member",
    "name": "Will Perkins",
    "title": "VP Media",
    "org": "Look Listen",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Todd Garland",
    "title": "Founder",
    "org": "BuySellAds",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Sebastien Bock",
    "title": "GVP Tech & Product Programmatic & Data",
    "org": "Rakuten Marketing",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Jon Malach",
    "title": "CEO",
    "org": "Native Ads, Inc.",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Francine Hardaway",
    "title": "Director of Marketing",
    "org": "ZEDO",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Henry Lau",
    "title": "Head of Partnerships",
    "org": "Instinctive",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Alex Calic",
    "title": "Chief Revenue Officer",
    "org": "The Media Trust",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Henrik Lohk",
    "title": "Co-founder",
    "org": "Adtoma AB",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Representative",
    "name": "James Sciolto",
    "title": "Co-Founder & Chief Marketing Officer",
    "org": "Bidio",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Nicole Pruess",
    "title": "Global Managing Director, Publisher Marketplace",
    "org": "Criteo",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Mat Bennett",
    "title": "Managing Director",
    "org": "OKO Digital",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Tom Gabriele",
    "title": "Co-Founder",
    "org": "WowYow Inc.",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Dan Greenberg",
    "title": "Founder & CEO",
    "org": "Sharethrough",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Eric Simon",
    "title": "COO",
    "org": "Skydeo",
  },
  {
    "group": "Creative Agents",
    "position": "Representative",
    "name": "Jon Lefley",
    "title": "Creative Director",
    "org": "Saatchi & Saatchi",
  },
  {
    "group": "Creative Agents",
    "position": "Member",
    "name": "Tobias Eidem",
    "title": "BrandGlobe and former chairman of Sweden’s KIA-index",
    "org": "BrandGlobe",
  },
  {
    "group": "Digital Rights Organizations",
    "position": "Member",
    "name": "Holmes Wilson",
    "title": "Co-Founder/Co-director",
    "org": "Fight for the Future",
  },
  {
    "group": "Digital Rights Organizations",
    "position": "Representative",
    "name": "Linda Sherry",
    "title": "Director National Priorities",
    "org": "Consumer Action",
  },
  {
    "group": "Digital Rights Organizations",
    "position": "Representative",
    "name": "Josh Golin",
    "title": "Executive Director",
    "org": "Campaign for a Commercial-Free Childhood",
  },
  {
    "group": "Publishers & Content Creators",
    "position": "Representative",
    "name": "Gavin Hall",
    "title": "CTO",
    "org": "TED",
  },
  {
    "group": "Publishers & Content Creators",
    "position": "Member",
    "name": "Nick Flood",
    "title": "Deputy Managing Director",
    "org": "Dennis Media",
  },
  {
    "group": "Publishers & Content Creators",
    "position": "Member",
    "name": "Christian Hendricks",
    "title": "Founder",
    "org": "Local Media Consortium",
  },
  {
    "group": "Publishers & Content Creators",
    "position": "Member",
    "name": "Scott Messer",
    "title": "VP Content Channels",
    "org": "Leaf Group",
  },
  {
    "group": "Publishers & Content Creators",
    "position": "Member",
    "name": "Loic Dussart",
    "title": "COO",
    "org": "KOL",
  },
  {
    "group": "Researchers & Academics",
    "position": "Member",
    "name": "Fran Howarth",
    "title": "Security Analyst",
    "org": "Bloor Analyst Group",
  },
  {
    "group": "Users",
    "position": "Representative",
    "name": "Zack Sinclair",
    "title": "Adblocking User",
    "org": False,
  },
  {
    "group": "Users",
    "position": "Member",
    "name": "Leli Schiestl",
    "title": "Adblocking User",
    "org": False,
  },
  {
    "group": "Digital Rights Organizations",
    "position": "Representative",
    "name": "Paul-Olivier Dehaye",
    "title": "Co-Founder",
    "org": "PersonalData.IO",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Michael Blend",
    "title": "Co-founder and President",
    "org": "System1",
  },
  {
    "group": "Advertisers",
    "position": "Member",
    "name": "Thomas Wrobel",
    "title": False,
    "org": "Trivago",
  },
  {
    "group": "Researchers & Academics",
    "position": "Member",
    "name": "Zubair Shafiq",
    "title": "Assistant Professor",
    "org": "Department of Computer Science, The University of Iowa, USA",
  },
  {
    "group": "Researchers & Academics",
    "position": "Representative",
    "name": "Arvind Narayanan",
    "title": "Assistant Professor",
    "org": "Princeton University",
  },
  {
    "group": "Researchers & Academics",
    "position": "Member",
    "name": "Lukasz Olejnik",
    "title": "Privacy Engineering Researcher",
    "org": "Independent",
  },
  {
    "group": "Advertisers",
    "position": "Member",
    "name": "Elizabeth Johnson",
    "title": "Director, Growth & Emerging Media",
    "org": "Ad Council",
  },
  {
    "group": "Ad Tech Agencies",
    "position": "Member",
    "name": "Marty Kratky-Katz",
    "title": "CEO",
    "org": "Blockthrough",
  },
  {
    "group": "User Agents",
    "position": "Representative",
    "name": "Michael O'Neill",
    "title": "Co-Founder & CTO",
    "org": "Baycloud Systems",
  },
] %}

{% macro member_list(group, position) -%}
  {% set count = [] %}
  {% for member in members %}
    {% if member.group == group and member.position == position %}
      {% if (count | length) == 0 %}
        <ul class="member-list">
      {% endif %}
      {% set _ = count.append(1) %}
      {% if member.title and member.org %}
        {% set separator = "," %}
      {% else %}
        {% set separator = "" %}
      {% endif %}
      <li>
        <strong>{{ member.name }}</strong><br>
        {% if member.title %}{{ member.title }}{% endif %}{{ separator }}
        {% if member.org %}<em>{{ member.org }}</em> {% endif %}
      </li>
    {% endif %}
  {% endfor %}
  {% if (count | length) > 0 %}
        </ul>
  {% endif %}
  {% if position == "Member" or (count | length) < 1 %}
    <p><a class="btn-primary" href="committee/apply">{{ "Apply" | translate("apply-button", "button label") }}</a></p>
  {% endif %}
{% endmacro %}

{% macro group_list(id, group) -%}
  <section class="group-list">
    <h4>{{ group | translate(id + "-heading", "heading") }}</h4>
    <dl>
      <dt>{{ "Representative" | translate("representative-heading", "heading") }}</dt>
      <dd>{{ member_list(group, "Representative") }}</dd>
    </dl>
    <dl>
      <dt>{{ "Members" | translate("members-heading", "heading") }}</dt>
      <dd>{{ member_list(group, "Member") }}</dd>
    </dl>
  </section>
{%- endmacro %}

{% macro coalition_section(id) -%}
  <section class="section">
    <h3 class="h2">{{ coalitions[id].title | translate("{{ id }}-heading", "heading") }}</h3>
    <p class="col-6">{{ coalitions[id].description | translate("{{ id }}-intro") }}</p>
    {% for group in coalitions[id].groups %}
      {{ group_list(id, group) }}
    {% endfor %}
  </section>
{% endmacro %}

<section class="container">
  <div class="row section">
    <div class="col-6">
      <h1>{{ "Members" | translate("members-heading", "heading") }}</h1>
      <p>{{ "The Acceptable Ads Committee is divided into three Coalitions, with each faction consisting of several Members. Each group includes a Representative and multiple supporting Members." | translate("members-intro-1") }}</p>
      <p>{{ "Below is a list of all current Representatives and supporting Members, broken down by Coalition.*" | translate("members-intro-2") }}</p>
      <p><small>{{ "*The Adblock Agent is a representative from an ad blocker product who attends meetings in an advisory role but does not have a vote and is not formally a member of the committee." | translate("members-intro-3") }}</small></small></p>
    </div>
    <aside class="col-6">
      <img id="committee-structure" class="block" alt="{{ "Acceptable Ads Committee Structure" | translate("featured-image-alt", "Image alt text") }}" src="img/jpg/acceptable-ads-committee-structure.jpg" />
    </aside>
  </div>
</section>

<section>

  <div class="bg-accent">
    <h2 class="center h3 p-y-md m-a-md">{{ "Meet the Acceptable Ads Committee" | translate("committee-heading", "heading") }}</h2>
  </div>

  <div class="container">
    {% for coalition in ["for-profit", "expert", "user-advocate"] %}
      {{ coalition_section(coalition) }}
    {% endfor %}
  </div>

</section>
