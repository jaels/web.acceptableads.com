title=Get whitelisted
description=Fill out the form to get whitelisted

<style type="text/css">
  #acceptable_ads, .showError #error-msg
  {
    display: block;
  }
  #WebToLeadForm.non-publishers #acceptable_ads,
  #WebToLeadForm.non-publishers .semioptional em, #error-msg
  {
    display: none;
  }
  .semioptional em
  {
    display: inline;
  }
</style>

<script type="text/javascript">
  function check_webtolead_fields()
  {
    if (document.getElementById("req_id") != null)
    {
      var required = document.getElementById("req_id").value;
      required = required.substring(0, required.lastIndexOf(';'));
      var requiredFields = required.split(';');

      // Some of the fields are optional only for publishers
      if (form.className == "non-publisher")
      {
        var semioptionalLabels = document.getElementsByClassName("semioptional");
        for (var i = 0; i < semioptionalLabels.length; i++)
        {
          requiredFields.push(semioptionalLabels[i].getAttribute("for"));
        }
      }

      var requireFilled = true;
      for (var i = 0; i < requiredFields.length; i++)
      {
        var field = document.getElementById(requiredFields[i]);
        if (field.value.length <= 0 || field.value == 0)
        {
          requireFilled = false;
          break;
        }
      }
      if (requireFilled)
      {
        document.WebToLeadForm.submit();
        return true;
      }
      else
      {
        showError("Please provide all the required fields");
        return false;
      }
      return false
    }
    else
      document.WebToLeadForm.submit();
  }

  function validateEmailAdd()
  {
    var emailField = document.getElementById("email1");
    if (emailField && emailField.value.length > 0 &&
        emailField.value.match(/^\w+(['\.\-\+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/) == null)
    {
      return false;
    }

    return true;
  }

  // Custom form validation
  var isHuman = false;
  var partnerTypeElement = null;
  var form = null;
  var requiredElement = null;
  var aaInput = null;
  var partnerType = "";
  var errorElement = null;

  function validateAndSubmit()
  {
    if (!isHuman)
      showError("To prevent spam the form has not been submitted");
    else
    {
      if (!validateEmailAdd())
      {
        showError("Not a valid email address");
        return false;
      }

      form.action = "https://eyeo.sugarondemand.com/index.php?entryPoint=WebToLeadCapture";
      check_webtolead_fields();
    }
  }

  function showError(msg)
  {
    if ("textContent" in errorElement)
      errorElement.textContent = msg;
    else // IE8
      errorElement.innerText = msg;

    form.className = form.className + " showError"
  }

  // Mark semioptional fields as required for non publishers group
  function updateSemioptional(required)
  {
    var semioptionalLabels = document.getElementsByClassName("semioptional");
    for (var i = 0; i < semioptionalLabels.length; i++)
    {
      var semioptionalId = semioptionalLabels[i].getAttribute("for");
      document.getElementById(semioptionalId).required = required;
    }
  }

  function updateForm()
  {
    var partnerType = partnerTypeElement[partnerTypeElement.selectedIndex];
    if (partnerType.id != "publishers")
    {
      form.className = "non-publishers";
      // Field is marked as required in CRM application, but it shouldn't
      if (!aaInput.value)
        aaInput.value = "none";

      updateSemioptional(true);
    }
    else
    {
      form.className = "";
      aaInput.value = aaInput.value == "none" ? "" : aaInput.value;

      updateSemioptional(false);
    }
  }

  document.addEventListener("DOMContentLoaded", function()
  {
    partnerTypeElement = document.getElementById("partner_type_c");
    form = document.getElementById("WebToLeadForm");
    requiredElement = document.getElementById("req_id");
    aaInput = document.getElementById("website_with_acceptable_ads_c");
    errorElement = document.getElementById("error-msg");

    // Get partner type from URL
    partnerType = location.search.split("type=")[1];
    var option = document.getElementById(partnerType);
    if (option)
    {
      document.getElementById(partnerType).selected = true;
      updateForm();
    }

    partnerTypeElement.addEventListener("change", function()
    {
      updateForm();
    });
    document.getElementById("email1").addEventListener("change", function()
    {
      // Spam protection
      isHuman = true;
      validateEmailAdd();

    }, false);
    form.addEventListener("submit", function(ev)
    {
      ev.preventDefault();
      validateAndSubmit();
      // prevent submition
      return false;
    }, false);
  }, false);
</script>

<div class="col-6 expand-on-tablet" markdown="1">

  <form id="WebToLeadForm" class="publishers section" method="POST" name="WebToLeadForm" markdown="1">

# {{get-whitelisted Get whitelisted}}

<div class="section" markdown="1">
{{make-web-better[subtitle] We love the web, but we think it can improve. Help us make it a better place!}}

{{about-acceptable-ads[paragraph] The Acceptable Ads initiative is simple: if your ads are nonintrusive and abide by the [Acceptable Ads criteria](about/criteria), they will be seen by a much larger audience, thus increasing your traffic and revenue. It's easy to join; many advertisers and publishers already benefit from the initiative. }}

{{before-applying[notice] **Important**: Before filling out the form please ensure your ads meet the [Acceptable Ads criteria](about/criteria). }}

<noscript>
  <p class="bg-error p-a-sm m-y-sm">
    {{enable-javascript This form requires JavaScript to be enabled to limit the amount of spam we get. Please consider enabling it for this page, or write an email to <a href="mailto:acceptableads@adblockplus.org">acceptableads@adblockplus.org</a>.}}
  </p>
</noscript>
</div>

<label for="partner_type_c">
  {{partner-type Partner Type}}
</label>
<select id="partner_type_c" name="partner_type_c" tabindex="1">
  <option id="publishers" selected="selected" value="Publisher">{{publishers Publishers}}</option>
  <option id="ad-networks" value="Ad Network">{{ad-networks Ad Networks}}</option>
  <option id="advertisers" value="Advertiser">{{advertisers Advertisers}}</option>
  <option id="ad-tech-suppliers" value="Ad Tech supplier">{{tech-suppliers Ad Tech suppliers}}</option>
</select>

<label class="semioptional" for="account_name" id="account_name_label">
  {{company-name Company Name}} <em>{{optional[Optional field label] (optional)}}</em>
</label>
<input id="account_name" type="text" name="account_name" />

<label for="first_name">
  {{first-name First Name}}
</label>
<input id="first_name" type="text" name="first_name" required />

<label for="last_name">
  {{last-name Last Name}}
</label>
<input id="last_name" type="text" name="last_name" required />

<label class="semioptional" for="title">
  {{job-title Job Title}} <em>{{optional}}</em>
</label>
<input id="title" type="text" name="title" />

<label for="website">
  {{website Company Website}} <em>{{optional}}</em>
</label>
<input id="website" type="text" name="website" />

<label for="email1">
  {{email Email Address}}
</label>
<input id="email1" type="email" name="email1" required />

<div id="acceptable_ads" markdown="1">
  <label for="website_with_acceptable_ads_c">
    {{aa-url1 Exact URL with acceptable ads}}
  </label>
  <input id="website_with_acceptable_ads_c" type="text" name="website_with_acceptable_ads_c" />
  <label for="second_website_with_acc_ads_c">
    {{aa-url2 Exact second URL with acceptable ads}} <em>{{optional}}</em>
  </label>
  <input id="second_website_with_acc_ads_c" type="text" name="second_website_with_acc_ads_c" />
  <label for="third_website_with_acc_ads_c">
    {{aa-url3 Exact third URL with acceptable ads}} <em>{{optional}}</em>
  </label>
  <input id="third_website_with_acc_ads_c" type="text" name="third_website_with_acc_ads_c" />
</div>

<label for="description">
  {{comments Comments}} <em>{{optional}}</em>
</label>
<textarea id="description" name="description" rows="14"></textarea>

<p class="m-y-md" markdown="1">
{{privacy-policy[Privacy policy note] **Privacy policy:** Your information will be stored and used only to handle this application and to respond to your inquiry. It will never be shared with any third party. If you would like to have your information removed from our database, please email us at [acceptableads@adblockplus.org](mailto:acceptableads@adblockplus.org).}}
</p>

error
{: #error-msg .bg-error .p-a-sm .m-y-sm }

<p class="inline-buttons">
<input class="btn-outline-primary" type="reset" name="reset" value="{{clear Clear}}" />
<input class="btn-primary" id="submit_button" type="submit" name="Submit" value="{{submit Submit}}" />
</p>

<input id="redirect_url" type="hidden" name="redirect_url" value="https://acceptableads.com/get-whitelisted/thank-you">
<input id="campaign_id" type="hidden" name="campaign_id" value="40e41f0e-ee21-9243-b374-57319a53b6f1">
<input id="assigned_user_id" type="hidden" name="assigned_user_id" value="652b2e55-db9d-c45d-e982-51e3ee13f0d8">
<input id="team_id" type="hidden" name="team_id" value="1">
<input id="team_set_id" type="hidden" name="team_set_id" value="Global">
<input id="req_id" type="hidden" name="req_id" value="partner_type_c;first_name;last_name;email1;website_with_acceptable_ads_c;">

  </form>
</div>
