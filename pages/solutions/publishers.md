title=Acceptable Ads for Publishers
description=Find out how to monetize your audience that uses an adblocker.
parent=solutions/index

<? include solutions/style ?>

# ![{{publishers-icon-alt[Publishers icon alt text] Publishers icon}}](/img/png/icon-publishers.png){: .card-icon } <br> {{publishers-heading[Publishers heading] Publishers}}

<div class="row section" markdown="1">
<div class="col-6" markdown="1">

{{publishers-p1[Publishers paragraph 1] As a publisher you might recognize a difference between page impressions and ad impressions on your website. This could be a result of visitors to your website that rejected to see your ads due to privacy or other concerns.}}

{{publishers-p2[Publishers paragraph 1] While there are many ways for them to achieve this, Adblock Plus is the only solution that offers its users an optional predefined whitelist and, equally, publishers a way to dynamically adjust their advertising to a level that is again acceptable to such users when they visit your websites.}}

{{publishers-p3[Publishers paragraph 1] When Acceptable Ads are enabled by the Adblock Plus users, certain forms of advertising will be shown to them if the ads comply with the strict Acceptable Ads criteria. Such ads need to be manually added to the whitelist by Eyeo as there is no automatic way to review that. As a publisher, you might want to apply to get whitelisted.}}

{{publishers-p4[Publishers paragraph 1] As a low effort alternative to your own application, you can also can implement ad tags from already whitelisted Acceptable Ads providers.}}

[{{publishers-get-whitelisted-button[Publishers get whitelisted button] Get whitelisted}}](get-whitelisted?type=publishers){: .btn-primary }
{: .btn-container }

</div>

<div class="col-6" markdown="1">
  <? include solutions/providers ?>
</div>

</div>
