title=Acceptable Ads and your ad blocker
description=Learn how to take control over your online ad experience with Acceptable Ads
custom=1

<div class="container section" markdown="1">
  <div class="col-6 expand-on-tablet" markdown="1">

# ![{{users-icon-alt[Users icon alt text] Users icon}}](/img/png/icon-users.png){: .card-icon } <br> {{users-heading[Page heading] Users }}

{{users-p1[Users paragraph 1] From its start in 2006 <fix>Adblock Plus</fix> blocked all the ads. But by 2011 we were starting to think this was a bad idea, so we asked our users if they thought so too. ​[They did](https://adblockplus.org/blog/adblock-plus-user-survey-results-part-0). In fact, a large majority said that they would be fine with seeing certain ads if they were not so intrusive. So, with their input, we developed standards for better ads. These became the first <fix>Acceptable Ads</fix> criteria, and as these criteria evolve users continue to be an irreplaceable part of the process.}}

{{users-p1[Users paragraph 1] But that doesn’t mean they have to be. <fix>Adblock Plus</fix> is endlessly and totally customizable, so users can ​[turn off <fix>Acceptable Ads</fix> and block all ads](#optout-aa). However, few do. After five years of running <fix>Acceptable Ads</fix>, users have demonstrated time and again that they want to play a role in the <fix>Acceptable Ads</fix> process, something the <fix>Acceptable Ads</fix> Committee will facilitate.}}

  </div>
</div>

<div class="bg-accent">
<div id="products" class="center group-container">
  <? include index/aa-products ?>
</div>
</div>

<div class="container section" markdown="1">

## {{disable-acceptable-ads[Disable Acceptable Ads heading] Disable Acceptable Ads}} {: #optout-aa }

{{disable-acceptable-ads-summary[Disable Acceptable Ads summary] Do you want to block all ads? No problem, you can disable <fix>Acceptable Ads</fix> at any time:}}

{{disable-acceptable-ads-chrome-dt[Disable Acceptable Ads on Chrome, Maxthon, Opera, Safari heading] <fix>Adblock Plus</fix> for <fix>Chrome</fix>, <fix>Maxthon</fix>, <fix>Opera</fix>, <fix>Safari</fix>}}
:    01. {{disable-acceptable-ads-chrome-dd-1[Disable Acceptable Ads on Chrome, Maxthon, Opera, Safari explanation line 1] Click the <fix>Adblock Plus</fix> icon and select **Options**.}}
     02. {{disable-acceptable-ads-chrome-dd-2[Disable Acceptable Ads on Chrome, Maxthon, Opera, Safari explanation line 2] Uncheck **Allow some nonintrusive advertising**.}}

{{disable-acceptable-ads-firefox-dt[Disable Acceptable Ads on Firefox heading] <fix>Adblock Plus</fix> for Firefox}}
:    01. {{disable-acceptable-ads-firefox-dd-1[Disable Acceptable Ads on Firefox explanation line 1] Click the <fix>Adblock Plus</fix> icon and select **Filter preferences**.}}
     02. {{disable-acceptable-ads-firefox-dd-2[Disable Acceptable Ads on Firefox explanation line 2] Uncheck **Allow some nonintrusive advertising**.}}

{{disable-acceptable-ads-ie-dt[Disable Acceptable Ads on Internet Explorer heading] <fix>Adblock Plus</fix> for <fix>Internet Explorer</fix>}}
:    01. {{disable-acceptable-ads-ie-dd-1[Disable Acceptable Ads on Internet Explorer explanation line 1] Click the <fix>Adblock Plus</fix> icon and select **Settings**.}}
     02. {{disable-acceptable-ads-ie-dd-2[Disable Acceptable Ads on Internet Explorer explanation line 2] Uncheck **Allow some nonintrusive advertising**.}}

{{disable-acceptable-ads-ios-dt[Disable Acceptable Ads on IOS heading] Adblock Plus for iOS}}
:    01. {{disable-acceptable-ads-ios-dd-1[Disable Acceptable Ads on IOS explanation line 1] Open <fix>Adblock Plus</fix> for <fix>iOS</fix> from the Home screen, tap the **Settings icon > <fix>Acceptable Ads</fix>**.
     02. {{disable-acceptable-ads-ios-dd-2[Disable Acceptable Ads on IOS explanation line 2] Uncheck **Allow some nonintrusive ads**.}}

{{disable-acceptable-ads-android-dt[Disable Acceptable Ads on Android] <fix>Adblock Browser</fix> for <fix>Android</fix>}}
:    01. {{disable-acceptable-ads-android-dd-1[Disable Acceptable Ads on Android explanation line 1] Open Settings, tap **Ad blocking > Configure <fix>Acceptable Ads</fix>**.}}
     02. {{disable-acceptable-ads-android-dd-2[Disable Acceptable Ads on Android explanation line 2] Uncheck **Allow some nonintrusive advertising**.}}

{{disable-acceptable-ads-abb-dt[Disable Acceptable Ads in Adblock Browser heading] <fix>Adblock Browser</fix> for <fix>iOS</fix>}}
:    01. {{disable-acceptable-ads-abb-dd-1[Disable Acceptable Ads in Adblock Browser explanation line 1] Open Settings, tap **<fix>Adblock Plus</fix> > <fix>Acceptable Ads</fix>**.}}
     02. {{disable-acceptable-ads-abb-dd-2[Disable Acceptable Ads in Adblock Browser explanation line 2] Uncheck **Allow some nonintrusive ads**.}}

</div>
